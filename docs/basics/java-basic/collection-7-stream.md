在Java中，集合和数组是我们经常会用到的数据结构，需要经常对他们做增、删、改、查、聚合、统计、过滤等操作。相比之下，关系型数据库中也同样有这些操作，但是在Java 8之前，集合和数组的处理并不是很便捷。

不过，这一问题在Java 8中得到了改善，Java 8 API添加了一个新的抽象称为流Stream，可以让你以一种声明的方式处理数据。本文就来介绍下如何使用Stream。特别说明一下，关于Stream的性能及原理不是本文的重点，如果大家感兴趣后面会出文章单独介绍。

### 为什么需要 Stream
Stream 作为 Java 8 的一大亮点，它与 java.io 包里的 InputStream 和 OutputStream 是完全不同的概念。它也不同于 StAX 对 XML 解析的 Stream，也不是 Amazon Kinesis 对大数据实时处理的 Stream。Java 8 中的 Stream 是对集合（Collection）对象功能的增强，它专注于对集合对象进行各种非常便利、高效的聚合操作（aggregate operation），或者大批量数据操作 (bulk data operation)。Stream API 借助于同样新出现的 Lambda 表达式，极大的提高编程效率和程序可读性。同时它提供串行和并行两种模式进行汇聚操作，并发模式能够充分利用多核处理器的优势，使用 fork/join 并行方式来拆分任务和加速处理过程。通常编写并行代码很难而且容易出错, 但使用 Stream API 无需编写一行多线程的代码，就可以很方便地写出高性能的并发程序。所以说，Java 8 中首次出现的 java.util.stream 是一个函数式语言+多核时代综合影响的产物。

### Stream介绍

Stream 使用一种类似用 SQL 语句从数据库查询数据的直观方式来提供一种对 Java 集合运算和表达的高阶抽象。

Stream API可以极大提高Java程序员的生产力，让程序员写出高效率、干净、简洁的代码。

这种风格将要处理的元素集合看作一种流，流在管道中传输，并且可以在管道的节点上进行处理，比如筛选，排序，聚合等。

#### 什么是流
Stream 不是集合元素，它不是数据结构并不保存数据，它是有关算法和计算的，它更像一个高级版本的 Iterator。原始版本的 Iterator，用户只能显式地一个一个遍历元素并对其执行某些操作；高级版本的 Stream，用户只要给出需要对其包含的元素执行什么操作，比如 “过滤掉长度大于 10 的字符串”、“获取每个字符串的首字母”等，Stream 会隐式地在内部进行遍历，做出相应的数据转换。

Stream 就如同一个迭代器（Iterator），单向，不可往复，数据只能遍历一次，遍历过一次后即用尽了，就好比流水从面前流过，一去不复返。

而和迭代器又不同的是，Stream 可以并行化操作，迭代器只能命令式地、串行化操作。顾名思义，当使用串行方式去遍历时，每个 item 读完后再读下一个 item。而使用并行去遍历时，数据会被分成多个段，其中每一个都在不同的线程中处理，然后将结果一起输出。Stream 的并行操作依赖于 Java7 中引入的 Fork/Join 框架（JSR166y）来拆分任务和加速处理过程。Java 的并行 API 演变历程基本如下：

* 1.0-1.4 中的 java.lang.Thread
* 5.0 中的 java.util.concurrent
* 6.0 中的 Phasers 等
* 7.0 中的 Fork/Join 框架
* 8.0 中的 Lambda

Stream 的另外一大特点是，数据源本身可以是无限的。

Stream有以下特性及优点：

*   无存储。Stream不是一种数据结构，它只是某种数据源的一个视图，数据源可以是一个数组，Java容器或I/O channel等。
*   为函数式编程而生。对Stream的任何修改都不会修改背后的数据源，比如对Stream执行过滤操作并不会删除被过滤的元素，而是会产生一个不包含被过滤元素的新Stream。
*   惰式执行。Stream上的操作并不会立即执行，只有等到用户真正需要结果的时候才会执行。
*   可消费性。Stream只能被“消费”一次，一旦遍历过就会失效，就像容器的迭代器那样，想要再次遍历必须重新生成。

我们举一个例子，来看一下到底Stream可以做什么事情：

![][1]￼

上面的例子中，获取一些带颜色塑料球作为数据源，首先过滤掉红色的、把它们融化成随机的三角形。再过滤器并删除小的三角形。最后计算出剩余图形的周长。

如上图，对于流的处理，主要有三种关键性操作：
* 流的创建(source)
* 中间操作（intermediate operation）
* 最终操作(terminal operation)。


### Stream的创建

在Java 8中，可以有多种方法来创建流。

* 从 Collection 和数组
> Collection.stream()     
> Collection.parallelStream()    
> Arrays.stream(T array) or Stream.of()   
* 从 BufferedReader   
> java.io.BufferedReader.lines()
* 静态工厂
> java.util.stream.IntStream.range()   
> java.nio.file.Files.walk()
* 自己构建
> java.util.Spliterator
* 其它
> Random.ints()   
> BitSet.stream()   
> Pattern.splitAsStream(java.lang.CharSequence)  
> JarFile.stream()




**1、通过Collection 和数组**

在Java 8中，除了增加了很多Stream相关的类以外，还对集合类自身做了增强，在其中增加了stream方法，可以将一个集合类转换成流。

    Stream<String> stream = strings.stream();
    // 1. Individual values
    Stream stream = Stream.of("a", "b", "c");
    // 2. Arrays
    String [] strArray = new String[] {"a", "b", "c"};
    stream = Stream.of(strArray);
    stream = Arrays.stream(strArray);
    // 3. Collections
    List<String> list = Arrays.asList(strArray);
    stream = list.stream();
  
 以上，通过一个已有的List创建一个流。除此以外，还有一个parallelStream方法，可以为集合创建一个并行流。这种通过集合创建出一个Stream的方式也是比较常用的一种方式. 

    //eg:parallelStream排序、取值实现
    List<Integer> transactionsIds = transactions.parallelStream().
    filter(t -> t.getType() == Transaction.GROCERY).
    sorted(comparing(Transaction::getValue).reversed()).
    map(Transaction::getId).
    collect(toList());

 
需要注意的是，对于基本数值型，目前有三种对应的包装类型 Stream：
* IntStream
* LongStream
* DoubleStream

Java 8 中还没有提供其它数值型 Stream，因为这将导致扩增的内容较多。而常规的数值型聚合运算可以通过上面三种 Stream 进行。

    //eg:数值流的构造
    IntStream.of(new int[]{1, 2, 3}).forEach(System.out::println);
    IntStream.range(1, 3).forEach(System.out::println);
    IntStream.rangeClosed(1, 3).forEach(System.out::println);


**2、通过Stream创建流**

可以使用Stream类提供的方法，直接返回一个由指定元素组成的流。

    Stream<String> stream = Stream.of("Hollis", "HollisChuang", "hollis", "Hello", "HelloWorld", "Hollis");
    

如以上代码，直接通过of方法，创建并返回一个Stream。


**3.自己生成流**

> 1. Stream.generate

通过实现 Supplier 接口，你可以自己来控制流的生成。这种情形通常用于随机数、常量的 Stream，或者需要前后元素间维持着某种状态信息的 Stream。把 Supplier 实例传递给 Stream.generate() 生成的 Stream，默认是串行（相对 parallel 而言）但无序的（相对 ordered 而言）。由于它是无限的，在管道中，必须利用 limit 之类的操作限制 Stream 大小。

    //eg:生成 10 个随机整数
    Random seed = new Random();
    Supplier<Integer> random = seed::nextInt;
    Stream.generate(random).limit(10).forEach(System.out::println);
    //Another way
    IntStream.generate(() -> (int) (System.nanoTime() % 100)).
    limit(10).forEach(System.out::println);

Stream.generate() 还接受自己实现的 Supplier。例如在构造海量测试数据的时候，用某种自动的规则给每一个变量赋值；或者依据公式计算 Stream 的每个元素值。这些都是维持状态信息的情形。

    //eg:自实现 Supplier
    Stream.generate(new PersonSupplier()).
    limit(10).
    forEach(p -> System.out.println(p.getName() + ", " + p.getAge()));
    private class PersonSupplier implements Supplier<Person> {
    private int index = 0;
    private Random random = new Random();
    @Override
    public Person get() {
    return new Person(index++, "StormTestUser" + index, random.nextInt(100));
    }
    }

> 2. Stream.iterate

iterate 跟 reduce 操作很像，接受一个种子值，和一个 UnaryOperator（例如 f）。然后种子值成为 Stream 的第一个元素，f(seed) 为第二个，f(f(seed)) 第三个，以此类推。

    //eg:生成一个等差数列
    Stream.iterate(0, n -> n + 3).limit(10). forEach(x -> System.out.print(x + " "));
    //0 3 6 9 12 15 18 21 24 27

与 Stream.generate 相仿，在 iterate 时候管道必须有 limit 这样的操作来限制 Stream 大小。


### Stream中间操作(Intermediate)
>Intermediate：一个流可以后面跟随零个或多个 intermediate 操作。其目的主要是打开流，做出某种程度的数据映射/过滤，然后返回一个新的流，交给下一个操作使用。这类操作都是惰性化的（lazy），就是说，仅仅调用到这类方法，并没有真正开始流的遍历。

Stream有很多中间操作，多个中间操作可以连接起来形成一个流水线，每一个中间操作就像流水线上的一个工人，每人工人都可以对流进行加工，加工后得到的结果还是一个流。

![][2]￼

以下是常用的中间操作列表:
>Intermediate：
map (mapToInt, flatMap 等)、 filter、 distinct、 sorted、 peek、 limit、 skip、 parallel、 sequential、 unordered

![][3]￼

**filter**

filter 方法用于通过设置的条件过滤出元素。以下代码片段使用 filter 方法过滤掉空字符串：

    List<String> strings = Arrays.asList("Hollis", "", "HollisChuang", "H", "hollis");
    strings.stream().filter(string -> !string.isEmpty()).forEach(System.out::println);
    //Hollis, , HollisChuang, H, hollis
    

**map：一对一映射**

map：作用就是把 input Stream 的每一个元素，映射成 output Stream 的另外一个元素。以下代码片段使用map操作的实例 ：

    1.求平方数
    List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
    numbers.stream().map( i -> i*i).forEach(System.out::println);
    //9,4,4,9,49,9,25
    
    2.单词转换为大写。
    List<String> output = wordList.stream().
    map(String::toUpperCase).collect(Collector.toList());

从上面例子可以看出，map 生成的是个 1:1 映射，每个输入元素，都按照规则转换成为另外一个元素。还有一些场景，是一对多映射关系的，这时需要 flatMap。
    
**flatMap：一对多**

    1.一对多
    Stream<List<Integer>> inputStream = Stream.of(
    Arrays.asList(1),
    Arrays.asList(2, 3),
    Arrays.asList(4, 5, 6)
    );
    Stream<Integer> outputStream = inputStream.
    flatMap((childList) -> childList.stream());

flatMap 把 input Stream 中的层级结构扁平化，就是将最底层元素抽出来放到一起，最终 output 的新 Stream 里面已经没有 List 了，都是直接的数字。
    

    1.一对多
    List<String> output = reader.lines().
    //每行的单词用 flatMap 整理到新的 Stream
    flatMap(line -> Stream.of(line.split(REGEXP))).
    filter(word -> word.length() > 0).
    collect(Collectors.toList());   
    

**limit/skip**

limit 返回 Stream 的前面 n 个元素；skip 则是扔掉前 n 个元素。以下代码片段使用 limit 方法保理4个元素：

    public void testLimitAndSkip() {
    List<Person> persons = new ArrayList();
    for (int i = 1; i <= 10000; i++) {
    Person person = new Person(i, "name" + i);
    persons.add(person);
    }
    List<String> personList2 = persons.stream().
    map(Person::getName).limit(10).skip(3).collect(Collectors.toList());
    System.out.println(personList2);
    }
    private class Person {
    public int no;
    private String name;
    public Person (int no, String name) {
    this.no = no;
    this.name = name;
    }
    public String getName() {
    System.out.println(name);
    return name;
    }
    }
    

**sorted**

对 Stream 的排序通过 sorted 进行，它比数组的排序更强之处在于你可以首先对 Stream 进行各类 map、filter、limit、skip 甚至 distinct 来减少元素数量后，再排序，这能帮助程序明显缩短执行时间：

    List<Person> persons = new ArrayList();
    for (int i = 1; i <= 5; i++) {
    Person person = new Person(i, "name" + i);
    persons.add(person);
    }
    List<Person> personList2 = persons.stream().limit(2).sorted((p1, p2) -> p1.getName().compareTo(p2.getName())).collect(Collectors.toList());
    System.out.println(personList2);
    //name2
    //name1
    //[stream.StreamDW$Person@6ce253f1, stream.StreamDW$Person@53d8d10a]
    

**distinct**

distinct主要用来去重，以下代码片段使用 distinct 对元素进行去重：

    eg:找出全文的单词，转小写，并排序
    List<String> words = br.lines().
    flatMap(line -> Stream.of(line.split(" "))).
    filter(word -> word.length() > 0).
    map(String::toLowerCase).
    distinct().
    sorted().
    collect(Collectors.toList());
    br.close();
    System.out.println(words);
    

接下来我们通过一个例子和一张图，来演示下，当一个Stream先后通过filter、map、sort、limit以及distinct处理后会发生什么。

代码如下：

    List<String> strings = Arrays.asList("Hollis", "HollisChuang", "hollis", "Hello", "HelloWorld", "Hollis");
    Stream s = strings.stream().filter(string -> string.length()<= 6).map(String::length).sorted().limit(3)
                .distinct();
    

过程及每一步得到的结果如下图：

![][4]￼

### Stream最终操作

Stream的中间操作得到的结果还是一个Stream，那么如何把一个Stream转换成我们需要的类型呢？比如计算出流中元素的个数、将流装换成集合等。这就需要最终操作（terminal operation）

最终操作会消耗流，产生一个最终结果。也就是说，在最终操作之后，不能再次使用流，也不能在使用任何中间操作，否则将抛出异常：

    java.lang.IllegalStateException: stream has already been operated upon or closed
    

俗话说，“你永远不会两次踏入同一条河”也正是这个意思。

* Terminal操作：
>forEach、 forEachOrdered、 toArray、 reduce、 collect、 min、 max、 count、 anyMatch、 allMatch、 noneMatch、 findFirst、 findAny、 iterator

常用的最终操作如下图：

![][5]￼

**forEach**

Stream 提供了方法 'forEach' 来迭代流中的每个数据。以下代码片段使用 forEach 输出了10个随机数：

    Random random = new Random();
    random.ints().limit(10).forEach(System.out::println);
    
需要注意的是，forEach 是 terminal 操作，因此它执行后，Stream 的元素就被“消费”掉了，你无法对一个 Stream 进行两次 terminal 运算。下面的代码是错误的：

    stream.forEach(element -> doOneThing(element));
    stream.forEach(element -> doAnotherThing(element));
 
 相反，具有相似功能的 intermediate 操作 peek 可以达到上述目的。如下是出现在该 api javadoc 上的一个示例。
    
    eg：peek 对每个元素执行操作并返回一个新的 Stream
    Stream.of("one", "two", "three", "four")
    .filter(e -> e.length() > 3)
    .peek(e -> System.out.println("Filtered value: " + e))
    .map(String::toUpperCase)
    .peek(e -> System.out.println("Mapped value: " + e))
    .collect(Collectors.toList());
    
==forEach 不能修改自己包含的本地变量值，也不能用 break/return 之类的关键字提前结束循环。==   

**findFirst**

这是一个 termimal 兼 short-circuiting 操作，它总是返回 Stream 的第一个元素，或者空。

**min/max**

min 和 max 的功能也可以通过对 Stream 元素先排序，再 findFirst 来实现，但前者的性能会更好，为 O(n)，而 sorted 的成本是 O(n log n)。同时它们作为特殊的 reduce 方法被独立出来也是因为求最大最小值是很常见的操作。

    1.找出最长一行的长度
    BufferedReader br = new BufferedReader(new FileReader("c:\\SUService.log"));
    br.lines().
    mapToInt(String::length).
    max.getAsInt();
    br.close();
    System.out.println(longest);

**reduce：合并**

这个方法的主要作用是把 Stream 元素组合起来。它提供一个起始值（种子），然后依照运算规则（BinaryOperator），和前面 Stream 的第一个、第二个、第 n 个元素组合。从这个意义上说，字符串拼接、数值的 sum、min、max、average 都是特殊的 reduce。例如 Stream 的 sum 就相当于

Integer sum = integers.reduce(0, (a, b) -> a+b); 或

Integer sum = integers.reduce(0, Integer::sum);

也有没有起始值的情况，这时会把 Stream 的前面两个元素组合起来，返回的是 Optional。

    // 字符串连接，concat = "ABCD"
    String concat = Stream.of("A", "B", "C", "D").reduce("", String::concat); 
    // 求最小值，minValue = -3.0
    double minValue = Stream.of(-1.5, 1.0, -3.0, -2.0).reduce(Double.MAX_VALUE, Double::min); 
    // 求和，sumValue = 10, 有起始值
    int sumValue = Stream.of(1, 2, 3, 4).reduce(0,     Integer::sum);
    // 求和，sumValue = 10, 无起始值
    sumValue = Stream.of(1, 2, 3, 4).reduce(Integer::sum).get();
    // 过滤，字符串连接，concat = "ace"
    concat = Stream.of("a", "B", "c", "D", "e", "F").
    filter(x -> x.compareTo("Z") > 0).
    reduce("", String::concat);
    
上面代码例如第一个示例的 reduce()，第一个参数（空白字符）即为起始值，第二个参数（String::concat）为 BinaryOperator。这类有起始值的 reduce() 都返回具体的对象。而对于第四个示例没有起始值的 reduce()，由于可能没有足够的元素，返回的是 Optional，请留意这个区别。

**count**

count用来统计流中的元素个数。

    List<String> strings = Arrays.asList("Hollis", "HollisChuang", "hollis","Hollis666", "Hello", "HelloWorld", "Hollis");
    System.out.println(strings.stream().count());
    //7
    

**collect**

collect就是一个归约操作，可以接受各种做法作为参数，将流中的元素累积成一个汇总结果：

    List<String> strings = Arrays.asList("Hollis", "HollisChuang", "hollis","Hollis666", "Hello", "HelloWorld", "Hollis");
    strings  = strings.stream().filter(string -> string.startsWith("Hollis")).collect(Collectors.toList());
    System.out.println(strings);
    //Hollis, HollisChuang, Hollis666, Hollis
  
 **补充**：流转换为其它数据结构 

     1. Array
    String[] strArray1 = stream.toArray(String[]::new);
    // 2. Collection
    List<String> list1 = stream.collect(Collectors.toList());
    List<String> list2 = stream.collect(Collectors.toCollection(ArrayList::new));
    Set set1 = stream.collect(Collectors.toSet());
    Stack stack1 = stream.collect(Collectors.toCollection(Stack::new));
    // 3. String
    String str = stream.collect(Collectors.joining()).toString();
 
  
  **用 Collectors 来进行 reduction 操作**
  
  java.util.stream.Collectors 类的主要作用就是辅助进行各类有用的 reduction 操作，例如转变输出为 Collection，把 Stream 元素进行归组。
  
  **groupingBy/partitioningBy**
  
  
    //eg1:按照年龄分组
    Map<Integer, List<Person>> personGroups =Stream.generate(new PersonSupplier()).
    limit(100).
    collect(Collectors.groupingBy(Person::getAge));
    Iterator it = personGroups.entrySet().iterator();
    while (it.hasNext()) {
    Map.Entry<Integer, List<Person>> persons = (Map.Entry) it.next();
    System.out.println("Age " + persons.getKey() + " = " + persons.getValue().size());
    }
    
    //eg2:按照未成年人和成年人归组
    Map<Boolean, List<Person>> children = Stream.generate(new PersonSupplier()).
    limit(100).
    collect(Collectors.partitioningBy(p -> p.getAge() < 18));
    System.out.println("Children number: " + children.get(true).size());
    System.out.println("Adult number: " + children.get(false).size());
    
在使用条件“年龄小于 18”进行分组后可以看到，不到 18 岁的未成年人是一组，成年人是另外一组。partitioningBy 其实是一种特殊的 groupingBy，它依照条件测试的是否两种结果来构造返回的数据结构，get(true) 和 get(false) 能即为全部的元素对象。
  
  
#### short-circuiting
还有一种操作被称为 short-circuiting。用以指：
* 对于一个 intermediate 操作，如果它接受的是一个无限大（infinite/unbounded）的 Stream，但返回一个有限的新 Stream。
* 对于一个 terminal 操作，如果它接受的是一个无限大的 Stream，但能在有限的时间计算出结果。

> Short-circuiting操作：
anyMatch、 allMatch、 noneMatch、 findFirst、 findAny、 limit

当操作一个无限大的 Stream，而又希望在有限时间内完成操作，则在管道内拥有一个 short-circuiting 操作是必要非充分条件。

**Match**

Stream 有三个 match 方法，从语义上说：

* allMatch：Stream 中全部元素符合传入的 predicate，返回 true
* anyMatch：Stream 中只要有一个元素符合传入的 *predicate，返回 true
* noneMatch：Stream 中没有一个元素符合传入的 predicate，返回 true

它们都不是要遍历全部元素才能返回结果。例如 allMatch 只要一个元素不满足条件，就 skip 剩下的所有元素，返回 false。对清单 13 中的 Person 类稍做修改，加入一个 age 属性和 getAge 方法。


    List<Person> persons = new ArrayList();
    persons.add(new Person(1, "name" + 1, 10));
    persons.add(new Person(2, "name" + 2, 21));
    persons.add(new Person(3, "name" + 3, 34));
    persons.add(new Person(4, "name" + 4, 6));
    persons.add(new Person(5, "name" + 5, 55));
    boolean isAllAdult = persons.stream().
    allMatch(p -> p.getAge() > 18);
    System.out.println("All are adult? " + isAllAdult);
    boolean isThereAnyChild = persons.stream().
    anyMatch(p -> p.getAge() < 12);
    System.out.println("Any child? " + isThereAnyChild);
    //All are adult? false
    //Any child? true


接下来，我们还是使用一张图，来演示下，前文的例子中，当一个Stream先后通过filter、map、sort、limit以及distinct处理后会，在分别使用不同的最终操作可以得到怎样的结果：

下图，展示了文中介绍的所有操作的位置、输入、输出以及使用一个案例展示了其结果。 ![][6]￼

### 总结
 
 总之，Stream 的特性可以归纳为：

* 不是数据结构

>- 它没有内部存储，它只是用操作管道从 source（数据结构、数组、generator function、IO channel）抓取数据。
>- 它也绝不修改自己所封装的底层数据结构的数据。例如 Stream 的 filter 操作会产生一个不包含被过滤元素的新 Stream，而不是从 source 删除那些元素。
>- 所有 Stream 的操作必须以 lambda 表达式为参数

* 不支持索引访问

>- 你可以请求第一个元素，但无法请求第二个，第三个，或最后一个。不过请参阅下一项。
>- 很容易生成数组或者 List

* 惰性化

>- 很多 Stream 操作是向后延迟的，一直到它弄清楚了最后需要多少数据才会开始。
>- Intermediate 操作永远是惰性化的。

* 并行能力

> 当一个 Stream 是并行化的，就不需要再写多线程代码，所有对它的操作会自动并行进行的。

*可以是无限的

>- 集合有固定大小，Stream 则不必。limit(n) 和 findFirst() 这类的 short-circuiting 操作可以对无限的 Stream 进行运算并很快完成。
 
 
 [1]: https://www.hollischuang.com/wp-content/uploads/2019/03/15521192454583.jpg
 [2]: https://www.hollischuang.com/wp-content/uploads/2019/03/15521194075219.jpg
 [3]: https://www.hollischuang.com/wp-content/uploads/2019/03/15521194556484.jpg
 [4]: https://www.hollischuang.com/wp-content/uploads/2019/03/15521242025506.jpg
 [5]: https://www.hollischuang.com/wp-content/uploads/2019/03/15521194606851.jpg
 [6]: https://www.hollischuang.com/wp-content/uploads/2019/03/15521245463720.jpg
 
 参考资料：https://www.ibm.com/developerworks/cn/java/j-lo-java8streamapi/?mhsrc=ibmsearch_a&mhq=%E4%B8%80%E4%B8%AA%E6%B5%81%E5%8F%AA%E8%83%BD%E6%9C%89%E4%B8%80%E4%B8%AA%20terminal%20%E6%93%8D%E4%BD%9C